/* ******************************************************************
 *  $Id: Editor.java,v 1.4 1999/02/06 18:14:47 spidey Exp spidey $
 *
 *  Node characteristics graphical editor.
 *  Copyright (C) 1999 Spidey <censored@domain.old>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *  See also <http://www.fsf.org>
 ********************************************************************
 * $Log: Editor.java,v $
 * Revision 1.4  1999/02/06 18:14:47  spidey
 * Now sets variables correctly
 * Uses arrays of checkboxes
 *
 * Revision 1.3  1999/02/06 14:28:00  spidey
 * Added:
 * - KeyListener: allows only numerics
 * - Pointer to the working Node
 * - ItemListener!
 * - Sets variables of Node directly now
 *
 * Revision 1.2  1999/02/06 03:57:10  spidey
 * Good implementation. No title to the frame...
 *
 * Revision 1.1  1999/02/06 02:57:41  spidey
 * Initial revision
 *
 *******************************************************************/
package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;

class Editor extends Dialog 
    implements ItemListener, 
               ActionListener,
               WindowListener,
               KeyListener {

    Node next, orig;
    TextField ratingF = new TextField("4",2);
    CheckboxGroup codeGrp,typeGrp;
    Checkbox codes[] = new Checkbox[4],
        types[] = new Checkbox[6];
    
    Editor(Frame f, Node n) {
        super(f,"Settings",true);
        setSize(200,200);
        GridBagLayout lay = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        GridBagConstraints b = new GridBagConstraints();
        setLayout(lay);
        Label
            ratingL = new Label("Security Rating"),
            codeL = new Label("Security Code"),
            typeL = new Label("Node Type");
        Button cancel = new Button("Cancel"),
            ok = new Button("OK");
        codeGrp = new CheckboxGroup();
        typeGrp = new CheckboxGroup();
        for (int i = 0; i < 4; i++) {
            codes[i] = new Checkbox(Node.getColorText(i),false,codeGrp);
            if ( i == n.getCode())
                codeGrp.setSelectedCheckbox(codes[i]);
        }
        for (int i = 0; i < 6; i++) {
            types[i] = new Checkbox(Node.getTypeText(i), false, typeGrp);
            if ( i == n.getType())
                typeGrp.setSelectedCheckbox(types[i]);
        }
        // Add the 2 top labels
        b.anchor = c.anchor = GridBagConstraints.NORTHWEST;
        b.weightx = c.weightx = 1;
        b.weighty = c.weighty = 1;
        b.gridwidth = GridBagConstraints.RELATIVE;
        c.gridwidth = GridBagConstraints.REMAINDER;
        add(typeL);
        lay.setConstraints(typeL,b);
        add(codeL);
        lay.setConstraints(codeL,c);
        for (int i = 0; i < 4; i++) {
            add(types[i]);
            lay.setConstraints(types[i],b);
            add(codes[i]);
            lay.setConstraints(codes[i],c);
        }
        add(types[4]);
        lay.setConstraints(types[4],b);
        add(ratingL);
        lay.setConstraints(ratingL,c);
        add(types[5]);
        lay.setConstraints(types[5],b);
        add(ratingF);
        lay.setConstraints(ratingF,c);
        add(cancel);
        lay.setConstraints(cancel,b);
        add(ok);
        lay.setConstraints(ok,c);
        for (int i = 0; i < 4; i++) {
            types[i].addItemListener(this);
            codes[i].addItemListener(this);
        }
        for (int i = 4; i < 6; i++)
            types[i].addItemListener(this);
        add(ok);
        ratingF.addKeyListener(this);
        ok.addActionListener(this);
        cancel.addActionListener(this);
        addWindowListener(this);
        next = n;
        orig = new Node(n);
        show();
        setVars();
    }

    public void setVars() {
        Checkbox b = codeGrp.getSelectedCheckbox();
        for (int i = 0; i < 4; i++)
            if (b.getLabel().equals(Node.getColorText(i)))
                next.setCode(i);
        b = typeGrp.getSelectedCheckbox();
        for (int i = 0; i < 6; i++)
            if (b.getLabel().equals(Node.getTypeText(i)))
                next.setType(i);
        try {
            next.setRating(Integer.parseInt(ratingF.getText()));
        } catch (NumberFormatException nexc) {}
    }
    public void itemStateChanged(ItemEvent i) {setVars();}
    public void actionPerformed(ActionEvent a) {
        if (a.getActionCommand().equals("OK")) {
            setVars();
            dispose();
        } else if (a.getActionCommand().equals("Cancel")) {
            next = orig;
            setVars();
            dispose();
        }
            
    }
    public void windowOpened(WindowEvent e){}
    public void windowClosing(WindowEvent e){dispose();}
    public void windowClosed(WindowEvent e){}
    public void windowIconified(WindowEvent e){}
    public void windowDeiconified(WindowEvent e){}
    public void windowActivated(WindowEvent e){}
    public void windowDeactivated(WindowEvent e){}
    public void keyTyped(KeyEvent e) {}
    public void keyPressed(KeyEvent e) {
//         char c = e.getKeyChar();
//         if (c < '9' && c > '0')
//             if (getParent() instanceof MatrixDesign)
//                 ((MatrixDesign)getParent()).message("Invalid key!");
    }
    public void keyReleased(KeyEvent e) {}
}
