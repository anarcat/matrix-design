/* ******************************************************************
 *  $Id: MatrixDesign.java,v 0.7 1999/02/12 17:04:23 spidey Exp spidey $
 *
 *  Shadowrun Matrix Computer systems Designer and Generator
 *  Copyright (C) 1999 Spidey <censored@domain.old>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *  See also <http://www.fsf.org>
 ********************************************************************
 * $Log: MatrixDesign.java,v $
 * Revision 0.7  1999/02/12 17:04:23  spidey
 * Added:
 * - Print capabilities, not tested
 * - Node numbering
 * - Handbook menu in help
 *
 * Revision 0.6  1999/02/10 02:48:54  spidey
 * Added:
 * - Minimum time for message() calls
 * - SetTitle() override to include "MatrixDesign:"
 * - Quit dialog when non-saved changes
 * Changed:
 * - [un]register(), quit() and such are now instance
 * - Message now hides correctly
 * - Corrected a bug in printPS()
 *
 * Revision 0.5  1999/02/09 03:33:48  spidey
 * Added:
 * - Save in PostScript
 * - Applet implementation
 * - beyond1dot1 boolean
 * - Open, save, save as, save as txt capabilities
 * - GetFilename method to get a filename from user
 * - ships unknown actionevents to Matrix
 * Removed:
 * - Edit menu, should be reimplemented!
 *
 * Revision 0.4  1999/02/06 18:38:56  spidey
 * Added:
 * - Edit menu: cut, copy, paste, and delete
 * - Help menu: readme file, warranty and copyright
 * Changed:
 * - messages is now local to each fram
 * Wishes:
 * - Select All in the Edit menu
 *
 * Revision 0.3  1999/02/04 16:12:48  spidey
 * Modified the code radically.
 * Now the package is gnu.MatrixDesign
 * This is now only the GUI.
 * The Matrix Canvas will handle other events.
 * Makefile refreshed
 *
 * Revision 0.2  1999/02/03 06:29:03  spidey
 * Added items to menu (view menu)
 * Possibility of removing the message part
 * Toolbar object declared. One static for all GUIs.
 * Handles windowEvents and focus events
 *
 * Revision 0.1  1999/02/02 21:05:24  spidey
 * Initial revision. Correct GUI. Menus with new, close, quit and
 * "copyright" that pops up the GPL. Keeps count of opened windows.
 *
 *******************************************************************/

package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import gnu.GraphPanel.PSGr;

public class MatrixDesign extends Frame 
    implements WindowListener,
               ItemListener,
               ActionListener {

    private Label messages = new Label("No messages");
    private String lastMessage = "";
    private long nextMessTime = 0;
    private Matrix sys;
    private static int winCount = 0;
    private static Window windows[] = new Window[10];
    public static boolean inAnApplet;
    private String fileName = "";
    static boolean beyond1dot1;

    final static String
        FILE = "File",
        NEW = "New",
        OPEN = "Open",
        SAVE = "Save",
        SAVEAS = "Save as...",
        SAVEPS = "Save as Postscript...",
        SAVETXT = "Save report as text...",
        PRINT = "Print Matrix",
        CLOSE = "Close",
        QUIT = "Quit",
        VIEW = "View",
        MESSAGES = "Messages",
        NODE_NUMBERS = "Node numbers",
        EDIT = "Edit",
        UNDO = "Undo",
        CUT = "Cut",
        COPY = "Copy",
        PASTE = "Paste",
        DELETE = "Delete",
        HELP = "Help",
        COPYRIGHT = "Copyright",
        WARRANTY = "Warranty",
        README = "Readme file",
        HANDBOOK = "Handbook";
    
    MatrixDesign() {
        super("MatrixDesign");
        beyond1dot1 = (System.getProperty("java.version")
                       .indexOf("1.1") != -1);
        setSize(450,450);
        show();
        sys = new Matrix(this);
        setLayout(new BorderLayout());
        if (beyond1dot1) {
            ScrollPane p = new ScrollPane();
            p.add(sys);
            add(p,"Center");
        } else
            add(sys,"Center");
        add(messages,"South");
        messages.setVisible(true);
        initMenus();
        initListen();
        register();
    }

    MatrixDesign(Matrix m) {
        super("MatrixDesign");
        beyond1dot1 = (System.getProperty("java.version")
                       .indexOf("1.1") != -1);
        setSize(400,400);
        show();
        sys = m;
        setLayout(new BorderLayout());
        if (beyond1dot1) {
            ScrollPane p = new ScrollPane();
            p.add(sys);
            add(p,"Center");
        } else
            add(sys,"Center");
        add(messages,"South");
        messages.setVisible(true);
        initMenus();
        initListen();
        register();
    }

    /** Adding necessary listeners */
    private final void initListen() {addWindowListener(this);}

    /** Add a MenuItem with the given string and ctrl- shortcut.*/
    private final void addMenuItem(Menu m,String s,char c) {
        MenuItem item = new MenuItem(s);
        if (c != 0)
            item.setShortcut(new MenuShortcut((int)c));
        item.setActionCommand(s);
        item.addActionListener((ActionListener)this);
        m.add(item);
    }

    /** Build the file menu */
    private final void initFileMenu(MenuBar mb) {
        Menu fileMenu = new Menu(FILE);
        addMenuItem(fileMenu,NEW,'n');
        addMenuItem(fileMenu,OPEN,'o');
        addMenuItem(fileMenu,CLOSE,'w');
        fileMenu.addSeparator();
        addMenuItem(fileMenu,SAVE,'s');
        addMenuItem(fileMenu,SAVEAS,'a');
        addMenuItem(fileMenu,SAVEPS,'r');
        addMenuItem(fileMenu,SAVETXT,'t');
        fileMenu.addSeparator();
        addMenuItem(fileMenu,PRINT,'p');
        fileMenu.addSeparator();
        addMenuItem(fileMenu,QUIT,'q');
        mb.add(fileMenu);
    }
  
    /** Builds the classical edit menu */
    private final void initEditMenu(MenuBar mb) {
//         Menu editMenu = (Menu) sys.editMenu;
//         mb.add(editMenu);
    }
    
    /** Builds the view menu */
    private final void initViewMenu(MenuBar mb) {
        Menu viewMenu = new Menu(VIEW);
        CheckboxMenuItem
            mess = new CheckboxMenuItem(MESSAGES,true),
            nums = new CheckboxMenuItem(NODE_NUMBERS,true);
        mess.setShortcut(new MenuShortcut((int)'m'));
        mess.setActionCommand(MESSAGES);
        mess.addItemListener((ItemListener)this);
        nums.setShortcut(new MenuShortcut((int)'u'));
        nums.setActionCommand(NODE_NUMBERS);
        nums.addItemListener((ItemListener)this);
        viewMenu.add(mess);
        viewMenu.add(nums);
        mb.add(viewMenu);
    }

    /** Build the help menu, and set it as 'help' */
    private final void initHelpMenu(MenuBar mb) {
        Menu helpMenu = new Menu(HELP);
        addMenuItem(helpMenu,COPYRIGHT,'l');
        addMenuItem(helpMenu,WARRANTY,(char)0);
        addMenuItem(helpMenu,README,'r');
        addMenuItem(helpMenu,HANDBOOK,'h');
        mb.add(helpMenu);
        mb.setHelpMenu(helpMenu);
    }
  
    /** Build all menus */
    private final void initMenus() {
        MenuBar menuBar = new MenuBar();
        initFileMenu(menuBar);
        initEditMenu(menuBar);
        initViewMenu(menuBar);
        initHelpMenu(menuBar);
        setMenuBar(menuBar);
    }

    /** Set visibility of message box */
    private void viewMessages(boolean b) {
        if (b) {
            add(messages,"South");
            validate();
        } else {
            remove(messages);
            validate();
        }
    }

    /** Display a certain message in the box */
    public void message(String m) {
        if (messages!= null) {
            if (nextMessTime <= System.currentTimeMillis())
                messages.setText(m);
        }
    }
    
    public void message(String m, long ms) {
        if (messages != null) {
            nextMessTime = System.currentTimeMillis() + ms;
            messages.setText(m);
        }
    }

    /**
     * Register the Window in the GUI.
     * This keeps tracks of opened windows and quits when the last one
     * is unregistered. 
     */
    private synchronized void register() {
        if (++winCount >= windows.length) {
            Window tmp[] = new Window[windows.length+5];
            for (int i = 0; i < windows.length; i++) {
                tmp[i] = windows[i];
            }
            windows = tmp;
        }
        windows[winCount] = (Window)this;
    }

    /**
     * Unregister the Window in the GUI.
     * This will call the quit() method if there's no more windows
     * left.
     */
    private synchronized void unregister() {
        if (--winCount <= 0) quit();
        else windows[winCount+1] = null;
    }

    // Use a dialog box to ask the user for the name of a file.
    private final String getFileName(String title,boolean load) {
        FileDialog dialog = new FileDialog(this,title);
        if ((fileName!=null) && (fileName!="")) {
            File f = new File(fileName);
            if (f.isFile()) {
                String t = f.getParent();
                dialog.setDirectory(t);
            }
        }
        dialog.setMode(load ? FileDialog.LOAD : FileDialog.SAVE);
        dialog.setTitle(title);
        dialog.show();
        dialog.setTitle(title);
        String s = dialog.getDirectory();
        if (s==null) s="";
        String f = dialog.getFile();
        if (f==null) return s;
        return s+f;
    }

    public void setTitle(String name) {
        name = name.substring(name.lastIndexOf('/')+1);
        super.setTitle("MatrixDesign: "+name);
    }

    /**
     * Quits the application.
     * Send a dispose to each registered window.
     */
    void quit() {
        if (sys.isChanged()) {
            QuitDialog q = new QuitDialog(this);
            if (!q.quit()) return;
            if (q.save()) save();
        }
        for (int i = 0; i < windows.length; i++)
            if (windows[i] != null)
                windows[i].dispose();
        if (!inAnApplet) System.exit(0);
    }

    private void close() {
        message("Closing...", 3000);
        dispose();
        unregister();
    }

    // Load a file.  The file is expected to contain output generated
    // from the graph's writeObject method.
    private final void open() {
        fileName=getFileName("Open",true);
        if (fileName==null) return;
        if (fileName.equals("")) return;
        message("Loading "+fileName);
        setCursor(Cursor.getPredefinedCursor(WAIT_CURSOR));
        repaint(0);
        readFile(fileName);
        setTitle(fileName);
        setCursor(Cursor.getPredefinedCursor(DEFAULT_CURSOR));    
        message("Loaded "+fileName, 2000);
        repaint(0);
    }

    // Save the serialized graph in a file, using the current
    // fileName, if it exists.
    private final void save() {
        if (sys.isChanged()) {
            sys.setChanged(false);
            if ("".equals(fileName)) {
                fileName=getFileName("Save",false);  
                repaint(0);
            }
            if (fileName==null) return;
            if (fileName.equals("")) return;
            message("Saving "+fileName);
            setCursor(Cursor.getPredefinedCursor(WAIT_CURSOR));
            repaint(0);
            writeFile(fileName,false);
            setTitle(fileName);
            setCursor(Cursor.getPredefinedCursor(DEFAULT_CURSOR));
            message("Saved.", 2000);
            repaint(0);
        } else 
            message("No changes to be saved.", 2000);
    }
  
    // Save the serialized graph in a file.
    private final void saveAs() {
        sys.setChanged(false);
        fileName=getFileName("Save As",false);  
        repaint(0);
        if (fileName==null) return;
        if (fileName.equals("")) return;
        setCursor(Cursor.getPredefinedCursor(WAIT_CURSOR));
        message("Saving As "+fileName);
        repaint(0);
        writeFile(fileName,false);
        setTitle(fileName);
        setCursor(Cursor.getPredefinedCursor(DEFAULT_CURSOR));
        message("Saved as "+ fileName, 2000);
        repaint(0);
    }

    // Save the PostScript version of the graph in a file.
    private final void savePS() {
        String printFileName = getFileName("Save as PostScript",false);  
        repaint(0);
        if (printFileName==null) return;
        if (printFileName.equals(""))return;
        if (printFileName.equals(fileName)) {
            System.err.println("I refuse to write PostScript on top of "+fileName+" .");
        } else {
            message("Saving in PostScript to "+printFileName);
            setCursor(Cursor.getPredefinedCursor(WAIT_CURSOR));
            repaint(0);
            printPS(printFileName);
            setCursor(Cursor.getPredefinedCursor(DEFAULT_CURSOR));
            message("Saved as "+printFileName, 3000);
            repaint(0);
        }
    }

    private final void saveTxt() {
        String printFileName = getFileName("Save as PostScript",false);  
        repaint(0);
        if (printFileName==null) return;
        if (printFileName.equals(""))return;
        if (printFileName.equals(fileName)) {
            System.err.println("I refuse to write PostScript on top of "+fileName+" .");
        } else {
            message("Saving in plain text to "+printFileName);
            setCursor(Cursor.getPredefinedCursor(WAIT_CURSOR));
            repaint(0);
            writeFile(printFileName,true);
            setCursor(Cursor.getPredefinedCursor(DEFAULT_CURSOR));
            message("Saved as "+printFileName, 3000);
            repaint(0);
        }
    }

    // Helper for open().
    private final void readFile(String file) {
        try {
            FileInputStream source = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(source);
            Container par = sys.getParent();
            par.remove(sys);
            sys = null;
            repaint(0);
            sys = ((Matrix)s.readObject());
            par.add(sys);
            source.close();
        } catch (Exception e) {
        }
        repaint();
    }

    // Write the graph to a file.
    // If text is true, write the result of graph.toString().
    // If text is false, write the serialized graph.
    private final void writeFile(String file,boolean text) {
        try {
            FileOutputStream dest = new FileOutputStream(file);
            if (text) {
                dest.write(sys.toString().getBytes());
            } else {
                ObjectOutputStream s = new ObjectOutputStream(dest);
                s.writeObject(sys);
                s.flush();
            }
            dest.close();
        } catch (Exception e) {
        }
    }

    private final void printPS(String psName) {
        String[] comments = {
            "",
            "This file was created by GraphPanel",
            "GraphPanel was written by David Binger <binger@centre.edu>",
            "of Centre College.",
            "",
            "This is a PostScript file.",
            "If it is sent to a PostScript printer or viewed using",
            "a PostScript viewing program like ghostview,",
            "you should see the image of the applet.",
            "You may also be able to save this file and insert",
            "it into the files that you produce with your word",
            "processor, if it knows how to deal with PostScript.",
            "",
            "The part of the GraphPanel program that generates this",
            "output is used with the permission of the original author,",
            "E.J. Friedman-Hill of Sandia National Labs.  Thanks, E.J.!",
            ""
        };
        try {
            FileOutputStream dest = new FileOutputStream(psName);
            Dimension s = getSize();
            //s.width -= (getInsets().left+getInsets().right);
            //s.height -= (getInsets().top+getInsets().bottom);
            //Point origin = new Point(getInsets().left,getInsets().top);
            PSGr psgr = new PSGr(dest,getGraphics(),s,new Point(0,0),comments);
            sys.paint(psgr);
            dest.write("\nshowpage\n".getBytes());
        } catch (Exception e) {
        }
    }

    private void print() {getToolkit().getPrintJob(this, getTitle(), null);}

    /**
     * Listens to menu events
     */
    public void actionPerformed(ActionEvent a) {
        String comm = a.getActionCommand();
        if (comm.equals(QUIT)) quit();
        else if (comm.equals(CLOSE)) close();
        else if (comm.equals(NEW)) {
            message("Creating a new design instance");
            new MatrixDesign();
            message("Created", 1000);
        } else if (comm.equals(SAVE)) save();
        else if (comm.equals(SAVEAS)) saveAs();
        else if (comm.equals(SAVEPS)) savePS();
        else if (comm.equals(SAVETXT)) saveTxt();
        else if (comm.equals(OPEN)) open();
        else if (comm.equals(PRINT)) print();
        else if (comm.equals(COPYRIGHT)) {
            message("Displaying copyright");
            new FileDisplayer(this, "GNU General Public Liscence", "COPYING");
            message("Done.", 1000);
        } else if (comm.equals(WARRANTY)) {
            message("Displaying warranty");
            new FileDisplayer(this, "No Warranty", "WARRANTY");
            message("Done.", 1000);
        } else if (comm.equals(README)) {
            message("Displaying README file");
            new FileDisplayer(this, "README File", "README");
            message("Done.", 1000);
        } else if (comm.equals(HANDBOOK)) {
            message("Displaying the handbook");
            new FileDisplayer(this, "MatrixDesign Handbook", "Handbook");
            message("Done.", 1000);
        } else sys.actionPerformed(a);
    }

    /**
     * Listen to check menu events
     */
    public void itemStateChanged(ItemEvent e) {
        Object o = e.getItem();
        if (o instanceof String)
            if (o.equals(MESSAGES))
                viewMessages
                    (e.getStateChange() == ItemEvent.SELECTED);
            else if (o.equals(NODE_NUMBERS))
                sys.setIDvisible
                    (e.getStateChange() == ItemEvent.SELECTED);
    }

    /**Handle a closing event. Close this window, simply.*/
    public void windowClosing (WindowEvent e) {close();}
    /**Nothing defined yet.*/
    public void windowOpened (WindowEvent e) {}
    /**Nothing defined yet.*/
    public void windowClosed (WindowEvent e) {}
    /**Nothing defined yet.*/
    public void windowIconified(WindowEvent e) {}
    /**Nothing defined yet.*/
    public void windowDeiconified(WindowEvent e) {}
    /**Nothing defined yet.*/
    public void windowActivated(WindowEvent e) {}
    /**Nothing defined yet.*/
    public void windowDeactivated(WindowEvent e) {}

    public static void main(String[] args) {new MatrixDesign();}

}
