package gnu.MatrixDesign;

import java.util.Random;

public class Data extends Object {

    private String name;
    private static final String NONE = "";
    private int value;
    private int size;
    private int color;
    private final static int SPECIAL = -1;
    private final static int values[][] = 
    {{500,  1000, 2500},
     {1000, 2500, 5000},
     {5000, 10000,50000},
     {10000,50000,100000}};
    private Random r = new Random();
    
    public Data() {
        name = NONE;
        size = ((r.nextInt()%6+1)+(r.nextInt()%6+1))*10;
        value = 0;
    }
    
    public Data(int color) {
        this(color, "");
    }

    public Data(int c, String n) {
        color = c;
        name = n;
        size = ((r.nextInt()%6+1)+(r.nextInt()%6+1))*10;
        computeValue();
    }

    public Data(int c, String n, int s) {
        color = c;
        name = n;
        size = s;
        computeValue();
    }

    public int computeValue() {
        if (color == Node.Blue) value = 0;
        else {
            color--;
            Random r = new Random();
            switch ((r.nextInt()%6+1)+(r.nextInt()%6+1)) {
            case 2:
            case 12:
                value = 0;
                break;
            case 3:
            case 4:
                value = values[0][color];
                break;
            case 5:
            case 6:
            case 7:
                value = values[1][color];
                break;
            case 8:
            case 9:
            case 10:
                value = values[2][color];
                break;
            case 11:
                value = values[3][color];
                break;
            }
        }
        return value *= size/10;
    }
    
    public Data(String n, int s, int v) {
        name = n;
        size = s;
        value = v;
    }

    public String getName() {return name;}
    public void setName(String n) {name = n;}
    public int getSize() {return size;}
    public void setSize(int s) {size = s;}
    public int getValue() {return value;}
    public void setValue(int v) {value = v;}

    public String toString() {
        return (name != NONE ? name + ": " : "")+ size + " Mp, " + 
            value + " nuyens";
    }
    
}
