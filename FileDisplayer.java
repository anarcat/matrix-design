package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class FileDisplayer extends Dialog implements Runnable, ActionListener {

    TextArea txt;
    String filename;

    FileDisplayer(Frame fr, String title, String f) {
        
        super(fr, title, false);
        filename = f;
        setLayout(new BorderLayout());
        txt = new TextArea("", 74,40);
        Button ok = new Button("OK");
        add(txt, "Center");
        txt.setVisible(true);
        add(ok,"South");
        ok.addActionListener(this);
        show();
        setSize(600,300);
        run();
    }

    public void run() {txt.setText(readFile(filename));}

    public void actionPerformed (ActionEvent e) {dispose();}
 
    private String readFile(String filename) {
        
        String ret = "",
            buff="";
        try {
            BufferedReader file = new BufferedReader
                (new FileReader(filename));
            while ((buff = file.readLine()) != null)
                ret += buff + '\n';
        } catch (Exception exc) {
            ret = null;
            System.err.println("Displayer: Can't read file : " + filename +
                               "Complete exception: " +exc);
        }
        return ret;
        
    }

}
