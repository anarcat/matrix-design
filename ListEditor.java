package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;

class ListEditor extends Dialog implements ActionListener {

    private List l;
    private boolean allRemoved;
    private int[] rems;
    private Object[] list;
    private boolean changed;

    ListEditor(Frame parent, Object[] lst, String title) {

        super(parent, title, true);
        changed = false;
        setLayout(new FlowLayout());
        list = lst;
        l = new List(5,true);
        for (int i = 0; i < list.length; i++) 
            if (list[i] != null)
                l.add(list[i].toString());
        Button rem = new Button("Remove"),
            cancel = new Button("Dismiss"),
            remAll = new Button("Remove all");
        add(l);
        add(cancel);
        add(rem);
        add(remAll);
        cancel.addActionListener(this);
        rem.addActionListener(this);
        remAll.addActionListener(this);
        pack();
        show();
    }

    public void actionPerformed(ActionEvent a) {
        String command = a.getActionCommand();
        if (command.equals("Remove")) {
            int sels[] = l.getSelectedIndexes();
            allRemoved = sels.length == l.getItemCount() ;
            rems = sels;
            changed = true;
        } else if (command.equals("Remove all"))
            changed = allRemoved = true;
        dispose();
    }

    int[] getRemoved() {return rems;}
    boolean allRemoved() {return allRemoved;}
    boolean isChanged() {return changed;}
}
