package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;

class QuitDialog extends Dialog implements ActionListener {
    
    private MatrixDesign parent;
    private boolean quit = false;
    private boolean save = false;

    QuitDialog(MatrixDesign p) {
        super((Frame)p, "Really quit??", true);
        parent = p;
        setLayout(new FlowLayout());
        add(new Label("There are unsaved changes. Do you still want yo quit?"));
        Button cancel, sq, quit;
        add(cancel = new Button("Cancel"));
        add(sq = new Button("Save & quit"));
        add(quit = new Button("Quit w/o saving"));
        cancel.addActionListener(this);
        sq.addActionListener(this);
        quit.addActionListener(this);
        pack();
        show();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Quit w/o saving")) quit = true;
        else if (e.getActionCommand().equals("Save & quit")) {
            save = true;
            quit = true;
        }
        dispose();
    }
    
    boolean quit() {return quit;}
    boolean save() {return save;}
}
