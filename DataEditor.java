package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;

class DataEditor extends Dialog implements ActionListener {

    private TextField name, size, value;
    private final static Label namel = new Label("Name:"),
        sizel = new Label("Size:"),
        valuel = new Label("Value:");
    private Node node;
    /** File presently edited */
    private Data file;
    private List list;
    private boolean canceled;

    DataEditor(Frame p, Node n) {
        super(p,"Editing file list", true);
        node  =  n;
        initList();
        name  = new TextField(10);
        size  = new TextField(2);
        value = new TextField(2);
        Button cancel = new Button("Cancel"),
            add = new Button("Add"),
            ok = new Button("OK");
        setLayout(new FlowLayout());
        add(list);
        add(namel);
        add(name);
        add(sizel);
        add(size);
        add(valuel);
        add(value);
        add(add);
        add(cancel);
        cancel.addActionListener(this);
        add.addActionListener(this);
        pack();
        show();
    }

    private void initList() {
        Data[] files = node.getFiles();
        list = new List(5,false);
        //remove(list);
        for (int i = 0; i < files.length; i++) 
            if (files[i] != null) {
                list.add(files[i].toString());
                System.err.println("adding"+files[i]);
            }
        //add(list);
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("Cancel")) {
            canceled = true;
            dispose();
        } else if (command.equals("Add")) {
            try {
                node.addFile
                    (new Data(name.getText(),
                              Integer.parseInt(size.getText()),
                              Integer.parseInt(value.getText())));
            } catch (Exception exc) {}
            dispose();
        }
    }

    public Node getNode() {return node;}
    public boolean canceled() {return canceled;}

}
