package gnu.MatrixDesign;

class IC extends Object {

    private int rating,color,type;
    
    private static final String colors[] = 
    {"White","Gray","Black"};
    private static final String types[] =
    {"Access", "Barrier", "Scramble",
     "Blaster", "Killer", "Tar baby", "Tar pit","Trace",
     "Trace & report", "Trace and dump", "Trace & burn",
     "Black IC"};
    public static final int
        ACCESS = 0,
        BARRIER = 1,
        SCRAMBLE = 2,
        BLASTER = 3,
        KILLER = 4,
        TAR_BABY = 5,
        TAR_PIT = 6,
        TRACE = 7,
        TRACE_REPORT = 8,
        TRACE_DUMP = 9,
        TRACE_BURN = 10,
        BLACK = 11,
        WHITE = 0,
        GRAY = 1;

    IC (int t, int r) {
        type = t;
        rating = r;
    }
        
    public static String getTypeText(int i) {
        i %= 12;
        return types[i];
    }
    public int getType(){return type;}
    public void setType(int t) {
        type = t;
        if (type < 0) type = 0;
        if (type > 11) type = 11;
    }
    public int getRating(){return rating;}
    public void setRating(int r) {rating = r;}
    public String getColor() {
        if (type < BLASTER)
            return colors[0];
        else if (type <= TRACE_BURN)
            return colors[1];
        else return colors[2];
    }
    public String toString() {
        if (type == 11) return "Black-"+rating;
        else return types[type]+"-"+rating;
    }
}
