/* ******************************************************************
 *  $Id: Matrix.java,v 1.13 1999/02/12 17:03:02 spidey Exp spidey $
 *
 *  The Matrix system itself. The nodes, vertices and event handlers.
 *  Copyright (C) 1999 Spidey <censored@domain.old>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *  See also <http://www.fsf.org>
 ********************************************************************
 * $Log: Matrix.java,v $
 * Revision 1.13  1999/02/12 17:03:02  spidey
 * Added:
 * - Node numbering
 * - Single CPU checking
 * Changed:
 * - add methods returns true or false if added or not
 * - ToString now efficient
 *
 * Revision 1.12  1999/02/10 02:44:02  spidey
 * Added:
 * - Changed notification
 * - Rating, comments editors working well
 * - Remove IC/file menus
 * - On the fly resizing of Matrix when getSize() is called
 *
 * Revision 1.11  1999/02/09 13:06:13  spidey
 * Corrected the edit rating/comments bug
 * Can add ICs
 *
 * Revision 1.10  1999/02/09 03:28:34  spidey
 * Added:
 * - Submenus to popup: Security Code, node type, add IC
 * - Items: add data file, rating and comments
 * - Possibility to add from submenu
 * - Edit rating & comments methods
 * - toString() method
 * Changed:
 * - Vertex are now drawn in top of nodes
 * - Can't drag too high or left
 *
 * Revision 1.9  1999/02/08 06:11:32  spidey
 * Added:
 * - popup menu
 * - serialization!
 * - ScrollPane caps!
 * Changed:
 * - Selection is only in Matrix now
 * - Applet implementation done with FrameStart.class
 * - paste capabilities improved
 *
 * Revision 1.8  1999/02/06 18:34:44  spidey
 * Added:
 * -Implemented a cut and paste, locally!
 * - AddNode(Node)
 * - copy(), cut(), paste(), delete(), great!
 * - Messages when hovering over nodes
 * Changed:
 * - Corrected a few bugs (null pointers..)
 * - Remove and removeVertex() are now correctly working
 *
 * Revision 1.7  1999/02/06 14:21:49  spidey
 * Now moves nodes by the center()
 * Background now black
 *
 * Revision 1.6  1999/02/06 02:23:44  spidey
 * Added:
 * - hits() method to check if a mouse event hits a node
 * - correct header
 * Modified:
 * - select() now returns the selected node
 * - mouseDragged: removed unecessary and unclear code
 * - mousePressed: cleared the code
 * - mouseReleased: idem
 * - removeVertex: now working properly
 *
 * Revision 1.5	 1999/02/06 01:46:41  spidey
 * Modified removeVertex to remove all vertex. Not working yet.
 * addVertex tries to remove existant identical vertex. Idem.
 * 
 * Revision 1.4  1999/02/06 00:51:52  spidey
 * Implemented vertices
 * Implemented node removing *concept*
 * Removes vertices in the process
 * Removed index use from arrays
 * Corrected a mouse drag bug
 * Backgroung is now white!
 *
 * Revision 1.3  1999/02/05 22:54:31  spidey
 * Correct version that eliminates the various tries
 * with multiple selections, and such.
 * The nodes now move and behave normally.
 *******************************************************************/
package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Date;

public class Matrix extends Canvas 
    implements ActionListener,
               MouseListener,
               MouseMotionListener,
               KeyListener,
               FocusListener,
               Serializable {

    /** List of vertices */
    private Vertex vlist[] = new Vertex[30];
    /** All the nodes on the matrix */
    private Node nodeList[] = new Node[10];
    /** Current selected node, and node from which we link to another */
    private Node selected, fromNode, cut;
    private MatrixDesign parent;
    //    private Rectangle selection;
    /** If we are currently dragging */
    private boolean dragging;
    private Dialog dialog;
    PopupMenu editMenu;
    private Point pastePoint;
    private boolean changed;
    private boolean IDvisible;
    private boolean hasCPU;

    public Matrix(MatrixDesign m) {
        super();
        parent = m;
        setVisible(true);
        setBackground(Color.white);
        editMenu = new PopupMenu("Edit");
        editMenu.add("Cut");
        editMenu.add("Copy");
        editMenu.add("Paste");
        editMenu.add("Delete");
        editMenu.addSeparator();
        Menu sec = new Menu("Security Code");
        for (int i = 0; i <= Node.Red; i++) {
            sec.add(Node.getColorText(i));
            sec.getItem(i).addActionListener(this);
        }
        editMenu.add(sec);
        Menu ty = new Menu("Node Type");
        for (int i = 0; i <= Node.SAN; i++) {
            ty.add(Node.getTypeText(i));
            ty.getItem(i).addActionListener(this);
        }
        editMenu.add(ty);
        editMenu.add("Rating...");
        editMenu.add("Comments...");
        editMenu.addSeparator();
        Menu ic = new Menu("Add IC");
        for (int i = 0; i <= IC.BLACK; i++) {
            ic.add(IC.getTypeText(i));
            ic.getItem(i).addActionListener(this);
        }
        editMenu.add(ic);
        editMenu.add("Remove IC...");
        editMenu.add("Add Data file...");
        editMenu.add("Remove file...");
        for (int i = 0; i < editMenu.getItemCount(); i++)
            editMenu.getItem(i).addActionListener(this);
        add(editMenu);
        fromNode = null;
        selected = null;
        initListen();
        changed = dragging = false;
        IDvisible = true;
    }
    
    private void initListen() {
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(this);
        addFocusListener(this);
    }

    public void paint(Graphics page) {
        page.setColor(Color.black);
        for (int i = 0; i < vlist.length; i++)
            if (vlist[i]!=null)
                vlist[i].paint(page);
        int nodeNumber = 1;
        for (int i = 0; i < nodeList.length; i++)
            if (nodeList[i]!=null) {
                nodeList[i].paint(page);
                if (IDvisible) {
                    nodeList[i].drawNumber(page,nodeNumber++);
                }
                if (selected != null && 
                    nodeList[i].equals(selected)) {
                    page.setColor(Color.black);
                    Rectangle sel = nodeList[i].getBounds();
                    page.drawRect(sel.x-1, sel.y-1, sel.width+2, sel.height+2);
                }
            }
    }

    void setChanged(boolean state) {changed = state;}
    boolean isChanged() {return changed;}

    void setIDvisible(boolean visibility) {
        IDvisible = visibility;
        repaint();
    }
    boolean getIDvisible() {return IDvisible;}

    private void remove(Node rem) {
        for (int i = 0; i < nodeList.length; i++)
            if (nodeList[i] != null  && nodeList[i].equals(rem)) {
                nodeList[i] = null;
                changed = true;
            }
        for (int i = 0; i < vlist.length; i++)
            if (vlist[i] != null && vlist[i].exists() &&
                (vlist[i].getStart().equals(rem) ||
                 vlist[i].getEnd().equals(rem))) {
                vlist[i] = null;
                changed = true;
            }
        rem = null;
    }

    private boolean removeVertex(Vertex rem) {
        boolean ret = false;
        for (int i = 0; i < vlist.length; i++)
            if (vlist[i] != null && vlist[i].equals(rem)) {
                vlist[i] = null;
                ret = true;
                parent.message("Removed vertex", 1000);
                changed = true;
            }
        rem = null;
        repaint();
        return ret;
    }

    private boolean add(Point p, int type) {
        Node n = new Node(p, type);
        return addNode(n);
    }
    private boolean add(Point p) {
        Node n = new Node(p);
        new Editor((Frame)parent,n);
        return addNode(n);
    }

    private boolean addNode(Node ad) {
        int insertIndex = 0;
        if (ad.getType() == Node.CPU)
            if (hasCPU) {
                parent.message("A system can only have one CPU", 3000);
                return false;
            } else hasCPU = true;
        for (insertIndex = 0; insertIndex < nodeList.length && 
                 nodeList[insertIndex] != null; insertIndex++);
        if (insertIndex >= nodeList.length) {
            parent.message("Growing nodelist array...", 3000);
            Node tmp[] = new Node[nodeList.length+5];
            for (int i = 0; i < nodeList.length; i++)
                tmp[i] = nodeList[i];
            nodeList = tmp;
        }
        nodeList[insertIndex] = ad;
        changed = true;
        if (pastePoint != null)
            ad.setCenter(pastePoint);
        repaint(); // should be repaint(x,y,dx,dy);
        return true;
    }

    private void addVertex() {
        if (!fromNode.equals(selected)) {
            int insertIndex = 0;
            Vertex toAdd = new Vertex(fromNode, selected);
            if (toAdd.exists()) {
                if (removeVertex(toAdd)) {
                    toAdd = null;
                    return;
                }
                for (insertIndex = 0; insertIndex < vlist.length &&
                         vlist[insertIndex] != null; insertIndex++);
                if (insertIndex >= vlist.length) {
                    parent.message("Growing Vertex list array...", 3000);
                    Vertex tmp[] = new Vertex[vlist.length+10];
                    for (int i = 0; i < vlist.length; i++)
                        tmp[i] = vlist[i];
                    vlist = tmp;
                }
                vlist[insertIndex] = toAdd;
                changed = true;
                repaint();
            } else {
                parent.message("Can't link a " + fromNode.getTypeText() + 
                               " to a " + selected.getTypeText(), 3000);
                toAdd = null;
            }
        }
    }

    private void unselect() {
        fromNode = selected = null;
        repaint();
    }
    
    private Node select(MouseEvent e) {
        for (int i = 0; i < nodeList.length; i++) {
            if (nodeList[i] != null &&
                nodeList[i].contains(e.getPoint()))
                selected = nodeList[i];
        }
        return selected;
    }
    
    private boolean hits (MouseEvent e) {
        for (int i = 0; i < nodeList.length; i++) {
            if (nodeList[i] != null &&
                nodeList[i].contains(e.getPoint()))
                return true;
        }
        return false;
    }

    private void cut()  {
        if (selected != null) {
            copy();
            delete();
        }
    }
    private void copy() {if (selected != null) cut = new Node(selected);}
    private void paste() {if (cut != null) addNode(new Node(cut));}
    private void delete() {
        remove(selected);
        repaint();
    }


    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("Cut")) cut();
        else if (command.equals("Copy")) copy();
        else if (command.equals("Cut")) cut();
        else if (command.equals("Paste")) paste();
        else if (command.equals("Delete")) delete();
        else if (command.equals("Rating...")) {
            //editRating();
            if (selected != null) {
                TextEditor ed = new TextEditor
                    ((Frame)parent,
                     ""+selected.getRating(),
                     "New rating:",
                     "Editing rating",
                     false);
                if (ed.isChanged()) {
                    selected.setRating(Integer.parseInt(ed.getText().trim()));
                    changed = true;
                }
            }
        } else if (command.equals("Comments...")) {
            //editComments();
            if (selected != null) {
                TextEditor ed = new TextEditor
                    ((Frame)parent,
                     selected.getComments(),
                     "Enter or modify comments",
                     "Editing comments",
                     true);
                if (ed.isChanged()) {
                    selected.setComments(ed.getText());
                    changed = true;
                }
            }
//                 selected.setComments
//                     (editText(selected.getComments(),
//                               "Enter or modify comments",
//                               "Editing comments",
//                               true));
        } else if (command.equals("Remove IC...")) {
            if (selected != null) {
                IC[] ics = selected.getICs();
                ListEditor li;
                if (ics != null) {
                    changed = true;
                    li = new ListEditor((Frame)parent,ics,"");
                    if (li.isChanged()) {
                        changed = true;
                        if (li.allRemoved()) selected.removeAllIC();
                        else {
                            int[] rems = li.getRemoved();
                            for (int i = 0; i < rems.length; i++)
                                selected.removeIC(ics[rems[i]]);
                        }
                    }
                }
            }
        } else if (command.equals("Add Data file...")) {
            if (selected != null) {
                DataEditor de;
                de = new DataEditor((Frame)parent, selected);
                if (!de.canceled()) {
                    System.err.println("not canceled");
                    selected = de.getNode();
                }
            }
        } else if (command.equals("Remove file...")) {
            if (selected != null) {
                Data[] files = selected.getFiles();
                ListEditor li;
                if (files != null) {
                    li = new ListEditor((Frame)parent,files,"");
                    if (li.isChanged()) {
                        changed = true;
                        if (li.allRemoved()) selected.removeAllFiles();
                        else {
                            int[] rems = li.getRemoved();
                            for (int i = 0; i < rems.length; i++)
                                selected.removeFile(files[rems[i]]);
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i <= Node.Red; i++) {
                if (command.equals(Node.getColorText(i))) {
                    if (selected == null) 
                        add(pastePoint);
                    else {
                        selected.setCode(i);
                        changed = true;
                    }
                    repaint();
                }
            }
            for (int i = 0; i <= Node.SAN; i++) {
                if (command.equals(Node.getTypeText(i))) {
                    if (selected != null) {
                        if (i == Node.CPU) {
                            if (hasCPU && !(selected.getType() == Node.CPU)) {
                                parent.message("A system can only have one CPU.",3000);
                                return;
                            } else hasCPU = true;
                        }
                        selected.setType(i);
                        changed = true;
                    } else 
                        add(pastePoint,i);
                    repaint();
                }
            }
            for (int i = 0; i <= IC.BLACK; i++)
                if (command.equals(IC.getTypeText(i)))
                    if (selected != null) {
                        TextEditor ed = new TextEditor
                            ((Frame)parent, " ","Enter rating for IC",
                             "Editing rating",false);
                        String rating = ed.getText();
                        if (ed.isChanged()) {
                            changed = true;
                            selected.addIC(new IC(i,Integer.parseInt(rating.trim())));
                        }
                    }
        }
    }

    public void mouseDragged(MouseEvent e){
        if (dragging) {
            if (e.isShiftDown() && selected != null) {
                Graphics page = getGraphics();
                Point p1 = selected.getCenter(), p2 = e.getPoint();
                page.setColor(Color.black);
                page.drawLine(p1.x, p1.y, p2.x, p2.y);
            } else if (selected != null)
                if (e.getX() > 0 && e.getY() > 0) {
                    changed = true;
                    selected.setCenter(e.getPoint());
                }
            repaint();
        }
        dragging = true;
        if (e.getX() > 0 && e.getY() > 0 &&
            !getBounds().contains(e.getPoint())) {
            Rectangle r = getBounds();
            r.add(e.getPoint());
            setBounds(r);
        }
    }
    public void mouseMoved(MouseEvent e){
        if (hits(e)) parent.message(select(e).toString());
    }
    
    public void mouseClicked(MouseEvent e){
        boolean hit = hits(e);
        if (e.getClickCount() > 1) {
            if (hit) {
                boolean isCPU = selected.getType() == Node.CPU;
                new Editor((Frame)parent, selected);
                if (selected.getType() == Node.CPU)
                    if (hasCPU)
                        if (!isCPU) selected.setType(Node.SPU);
                        else;
                    else hasCPU = true;
                changed = true;
            }
            else add(e.getPoint());
            unselect();
        } else {
            unselect();
            if (select(e) == null)
                unselect();
        }
        dragging = false;
    }
    public void mousePressed(MouseEvent e){
        pastePoint = e.getPoint();
        unselect();
        if (hits(e))
            if (e.isShiftDown() || e.getModifiers() == e.BUTTON2_MASK)
                fromNode = select(e);
            else select(e);
        if (e.isAltDown() || ((boolean) (e.getModifiers() == e.BUTTON3_MASK) ))
            editMenu.show(this,e.getX(), e.getY());
    }
    public void mouseReleased(MouseEvent e){
        if (dragging && fromNode != null && e.isShiftDown() && hits(e)) {
            select(e);
            addVertex();
        }
        dragging = false;
    }
    public void mouseEntered(MouseEvent e){}
    public void mouseExited(MouseEvent e) {}
    
    
    public void keyTyped(KeyEvent e) {System.err.println(e);}
    public void keyPressed(KeyEvent e){System.err.println(e);}
    public void keyReleased(KeyEvent e) {System.err.println(e);}
    
    public void focusGained(FocusEvent e) {repaint();}
    public void focusLost(FocusEvent e){}
    
    public Dimension getSize() {
        Rectangle totalSize = new Rectangle(0,0,0,0);
        for (int i = 0; i < nodeList.length; i++)
            if (nodeList[i] != null)
                totalSize = totalSize.union(nodeList[i]);
        totalSize.add(new Point(0,0));
        setSize(totalSize.getSize());
        return super.getSize();
    }
    
    public Dimension getMinimumSize() {return getSize();}
    public Dimension getPreferredSize() {return getSize();}

    public String toString() {
        String ret = 
            "Matrix Design: report on Matrix Status on " + new Date() + '\n';
        int nodeNumber = 1;
        for (int i = 0; i < nodeList.length ; i++)
            if (nodeList[i] != null)
                ret += "Node "+(nodeNumber++)+": "+nodeList[i].toString()+"\n\n";                
        return ret;
    }
}
