# ../.. in the CLASSPATH makes it easier to 
# make gnu.* packages.  Note that separators in
# CLASSPATH are semicolons in DOS, but colons on Unix.

JAVADIR= /usr/local/java/
CLASSPATH = '${JAVADIR}lib/classes.zip:../../'

RUNCLASSPATH = '${JAVADIR}lib/classes.zip:MatrixDesign.jar'

all:  test

classes: Data.class IC.class Node.class Vertex.class ListEditor.class \
	 DataEditor.class TextEditor.class FrameStart.class FileDisplayer.class \
	 QuitDialog.class Matrix.class MatrixDesign.class 

applet:  MatrixDesignAppl.jar
	${JAVADIR}bin/appletviewer appletTest.html

%.class: %.java
	${JAVADIR}bin/javac -classpath $(CLASSPATH) $<

test: MatrixDesign.jar 
	${JAVADIR}bin/java -classpath $(RUNCLASSPATH) gnu.MatrixDesign.MatrixDesign

clean:
	rm -f *class

jar: MatrixDesign.jar

SRCjar: MatrixDesignSRC.jar

MatrixDesignAppl.jar: classes
	(cd ../..; /usr/local/java/bin/jar cvf gnu/MatrixDesign/MatrixDesignAppl.jar gnu/MatrixDesign/*.class \
	gnu/GraphPanel/PSGr.class gnu/MatrixDesign/COPYING gnu/MatrixDesign/README gnu/MatrixDesign/WARRANTY)


MatrixDesignSRC.jar: *.java Makefile
	(cd ../..; /usr/local/java/bin/jar cvf gnu/MatrixDesign/MatrixDesignSRC.jar gnu/MatrixDesign/*.java gnu/MatrixDesign/Makefile gnu/MatrixDesign/COPYING gnu/MatrixDesign/README gnu/MatrixDesign/WARRANTY)

MatrixDesign.jar: classes
	(cd ../..; /usr/local/java/bin/jar cvf gnu/MatrixDesign/MatrixDesign.jar \
	gnu/GraphPanel/PSGr.class \
	gnu/MatrixDesign/*class \
	gnu/MatrixDesign/COPYING gnu/MatrixDesign/README gnu/MatrixDesign/WARRANTY \
	gnu/MatrixDesign/Handbook)
