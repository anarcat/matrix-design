package gnu.MatrixDesign;

import java.awt.*;
import java.awt.event.*;

class TextEditor extends Dialog implements ActionListener {

    TextComponent tc;
    boolean ok;
    String orig;

    TextEditor(Frame parent, String text, String label, String title, boolean area) {
        super(parent, title, true);
        ok = false;
        orig = text;
        if (area) tc = new TextArea(text);
        else tc = new TextField(text);
        Button ok = new Button("OK"), cancel = new Button("Cancel");
        Label lab = new Label(label);
        if (area) {
            setLayout(new BorderLayout());
            add(lab,"North");
            add(tc, "Center");
            Panel so = new Panel();
            so.add(cancel);
            so.add(ok);
            add(so,"South");
        } else {
            setLayout(new FlowLayout());
            add(lab);
            add(tc);
            add(cancel);
            add(ok);
        }
        cancel.addActionListener(this);
        ok.addActionListener(this);
        pack();
        show();
    }
    public void actionPerformed(ActionEvent a) {
        dispose();
        ok = (a.getActionCommand().equals("OK"));
    }
    
    public boolean isChanged() {return ok;}

    public String getText() {
        if (ok) return tc.getText();
        else return orig;
    }
}
