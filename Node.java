/* ******************************************************************
 *  $Id: Node.java,v 1.11 1999/02/10 02:50:25 spidey Exp spidey $
 *
 *  This a Shodowrun Matrix Systems Node.
 *  Copyright (C) 1999 Spidey <censored@domain.old>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *  See also <http://www.fsf.org>
 ********************************************************************
 * $Log: Node.java,v $
 * Revision 1.11  1999/02/10 02:50:25  spidey
 * Added removeFile(index) getICs/Files
 *
 * Revision 1.10  1999/02/09 03:38:31  spidey
 * Added:
 * - IC list, File list and Comments field
 * - Node(p,type) constructor
 * - add|removeIC(),add|removeFile()
 * Removed:
 * - parent
 *
 * Revision 1.9  1999/02/07 15:36:44  spidey
 * Added:
 * - iop drawing
 * Removed
 * - selection methods are now in Matrix
 *
 * Revision 1.8  1999/02/06 18:13:49  spidey
 * Added:
 * - Selected capability select() and unselect()
 * - Copy constructor
 * - Static methods to find color names and node names..
 * Changed:
 * - Better toString()
 * - Enlarged a little CPU and SPU
 *
 * Revision 1.7  1999/02/06 15:14:34  spidey
 * Added:
 * - Connectivity to others node checking connects(Node)
 * - Refined toString()
 * - getColor() getColorName()
 * Changed:
 * - Integer codes for type finals
 *
 * Revision 1.6  1999/02/06 14:18:55  spidey
 * Returned to non-polygon-oriented code
 * Many changes into the code.
 *
 * Revision 1.5  1999/02/06 11:16:19  spidey
 * Merde.
 *
 * Revision 1.4  1999/02/06 09:14:24  spidey
 * Bad version tried a concept that led to multiple inheritance. :(
 *
 * Revision 1.3  1999/02/05 23:20:20  spidey
 * paintShape is now 'paint'
 * edit() empty.
 *
 * Revision 1.2  1999/02/05 14:13:07  spidey
 * Node is now a Rectangle
 * No more a awt component.
 * Contains a paint method, however, that paints with the
 * specified page.
 *
 * Revision 1.1  1999/02/04 16:41:00  spidey
 * Initial revision
 *
 *******************************************************************/

package gnu.MatrixDesign;

import java.awt.*;

/** Node construct */
class Node extends Rectangle {

    static final int
        CPU = 0,
        DS  = 1,
        IOP = 2,
        SN  = 3,
        SPU = 4,
        SAN = 5,
        Blue   = 0,
        Green  = 1,
        Orange = 2,
        Red    = 3;
    private static final String shortNames[] = 
    {"CPU","DS","IOP","SN","SPU","SAN"};
    private static final String longNames[] = 
    {"Central Processing Unit","Datastore","I/O Port",
     "Slave Node","Sub-Processing Unit","System Access Node"};
    private static final boolean connections[][] = 
    {{false, true, true, true, true, true}, // CPU to all save itself
     {true, true, false, false, true, false}, // DS to CPU, DS, SPU
     {true, false, false, false, true, false}, // IOP to CPU, SPU
     {true, false, false, false, true, false}, // SN to CPU, SPU
     {true, true, true, true, true, true}, // SPU to all
     {true, false, false, false, true, true}}; // SAN to CPU, SPU
    private static Color[] colors = 
    {Color.blue, Color.green, Color.orange, Color.red};
    private int rating, code, type;
    private IC ices[] = new IC[5];
    private Data files[] = new Data[5];
    private String comments = "";

    public Node () {super();}

    public Node (Node n) {
        x = n.x; y = n.y; height = n.height; width = n.width;
        rating = n.rating;
        code = n.code;
        type = n.type;
    }

    public Node (Point p) {
        this(p,SPU);
    }

    public Node (Point p, int type) {
        super(p);
        x = p.x;
        y = p.y;
        setType(type);
    }

    public String toString() {
        String ret = "";
        ret += getColorText()+ ' ' +  getTypeText() +
            ", rating " + ((getRating() > 0)?
                           (""+getRating()):
                           "invalid");
        ret += "\n\tIC:\n";
        for (int i = 0; i < ices.length; i++)
            if (ices[i] != null)
                ret += "\t\t" + ices[i].toString() + '\n';
        ret += "\tFiles:\n";
        for (int i = 0; i < files.length; i++)
            if (files[i] != null)
                ret += "\t\t" + files[i].toString() + '\n';
        if (!comments.equals(""))
            ret += "\tComments:\n" + comments;
        return ret;
    }

    public void paint(Graphics page) {
        
        Polygon draw = new Polygon();
        page.setColor(getColor());
        switch(type) {
        case SAN:
        case DS:
            page.fillRect(x,y,width,height);
            break;
        case IOP:
            draw.addPoint(x,y+height);
            draw.addPoint(x+width, y+height);
            draw.addPoint(x+width/2, y);
            page.fillPolygon(draw);
            break;
        case SN:
            page.fillOval(x,y,width,height);
            break;
        case CPU:
            page.fillPolygon(hexagone(x+2,y+2,height-4));
            page.drawPolygon(hexagone(x,y,height));
            break;
        case SPU:
            page.fillPolygon(hexagone(x,y,height));
            break;
        }
    }

    public void drawNumber(Graphics page, int i) {
        page.setColor(Color.black);
        int ascent = page.getFontMetrics().getAscent();
        page.drawString(""+i,getCenter().x-4, y+height+ascent);
    }

    public Point getCenter() 
    {return new Point(x + width/2, y + height/2);}
    public void  setCenter(Point p) 
    {setLocation(p.x - width/2, p.y - height/2);}
    public void  setCode(int c)   {code = c;}
    public int   getCode()        {return code;}
    public void  setType(int c)   {
        type = c;
        switch(type) {
        case SAN:
            height = 20;
            width = 40;
            break;
        case IOP:
        case SN:
        case DS:
            width = height = 20;
            break;
        case CPU:
            height = 25;
            width = 27;
            break;
        case SPU:
            height = 21;
            width = 23;
            break;
        }
    }
    public int   getType()        {return type;}
    public String   getTypeText() {return (shortNames[type]);}
    public static String   getTypeText(int i) 
    {return (shortNames[i]);}
    public String   getTypeLongText() {return (longNames[type]);}
    public void  setRating(int c) {rating = c;}
    public int   getRating()      {return rating;}
    Color getColor()      {return colors[code];}
    String getColorText() {return getColorText(code);}
    static String getColorText(int code) {
        switch (code) {
        case Blue:
            return "Blue";
        case Green:
            return "Green";
        case Orange:
            return "Orange";
        case Red:
            return "Red";
        default:
            return "Unknown color";
        }
    }

    public void setComments(String s) {comments = s;}
    public String getComments() {return comments;}

    public IC[] getICs() {return ices;}
    public Data[] getFiles() {return files;}

    public void removeAllIC() {ices = new IC[5];}
    public void removeIC(IC ic) {
        for (int i = 0; i < ices.length; i++) 
            if (ices[i] != null && ices[i].equals(ic))
                ices[i] = null;
    }
    public void addIC(IC ic) {
        int insertIndex = 0;
        for (insertIndex = 0; insertIndex < ices.length &&
                 ices[insertIndex] != null;insertIndex++);
        if (insertIndex >= ices.length) {
            IC tmp[] = new IC[insertIndex+5];
            for (int i = 0; i < ices.length; i++)
                tmp[i] = ices[i];
            ices = tmp;
        }
        ices[insertIndex] = ic;
    }

    public void removeAllFiles() {files = new Data[5];}
    public void removeFile(Data file) {
        for (int i = 0; i < files.length; i++)
            if (files[i] != null && files[i].equals(file))
                files[i] = null;
    }
    public void removeFile(int i) 
    {files[i] = null;}
    public void addFile(Data file) {
        int insertIndex = 0;
        for (insertIndex = 0; insertIndex < files.length &&
                 files[insertIndex] != null;insertIndex++);
        if (insertIndex >= files.length) {
            Data tmp[] = new Data[insertIndex+5];
            for (int i = 0; i < files.length; i++)
                tmp[i] = files[i];
            files = tmp;
        }
        files[insertIndex] = file;
    }

    public boolean connects(Node n) 
    {return connections[type][n.getType()];}

    public static Polygon hexagone(int x, int y, int height ) {
        Polygon hexa = new Polygon();
        final int
            //  a is a variable
            A = (int) Math.rint(height/2 * Math.tan(Math.PI/6)),
            // aaa is 3 times a
            AAA = (int) Math.rint(3*height/2 * Math.tan(Math.PI/6));
        hexa.addPoint(x,y + height/2);
        hexa.addPoint(x + A, y );
        hexa.addPoint(x + AAA, y );
        hexa.addPoint(x + (int) Math.rint
                      (2 * height * Math.tan(Math.PI / 6)),
                      y + height/2);
        hexa.addPoint(x + AAA, y + height);
        hexa.addPoint(x + A, y + height);
        return hexa;
    }
}
