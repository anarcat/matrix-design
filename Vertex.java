package gnu.MatrixDesign;

import java.awt.*;
import java.io.Serializable;

class Vertex extends Object 
implements Serializable {

    private Node start, end;

    Vertex() {start = end = null;}
    Vertex(Node s, Node e) {
        if (s.connects(e)) {
            start = s;
            end = e;
        } else 
            end = start = null;
    }

    public void paint(Graphics page) {
        Point p1 = start.getCenter(), p2 = end.getCenter();
        page.drawLine(p1.x, p1.y, p2.x, p2.y);
    }
    Node getStart() {return start;}
    Node getEnd() {return end;}
    boolean exists() {return (start != null) && (end != null);}
    
    /**
     * Override the equals method of Object.  
     * It is equal if the Object.equals() is true or if the start, and 
     * the end of the vertice are the same. It is certainly not equal 
     * if if is not an instanceof Vertex.
     */
    public boolean equals(Vertex obj) {
        return (start.equals(obj.getStart()) &&
                end.equals(obj.getEnd())) ||
            (end.equals(obj.getStart()) &&
             start.equals(obj.getEnd()));
    }
}
